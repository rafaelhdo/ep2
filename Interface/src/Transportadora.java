
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rafael
 */
public class Transportadora {
    ArrayList<Carreta> carretas;
    ArrayList<Van> vans;
    ArrayList<Carro> carros;
    ArrayList<Moto> motos;

    public Transportadora() {
        carretas = new ArrayList();
        vans = new ArrayList();
        carros = new ArrayList();
        motos = new ArrayList();
    }

    public ArrayList<Carreta> getCarretas() {
        return carretas;
    }

    public ArrayList<Carro> getCarros() {
        return carros;
    }

    public ArrayList<Moto> getMotos() {
        return motos;
    }

    public ArrayList<Van> getVans() {
        return vans;
    }
    
    public int get_size_carretas() {
        return carretas.size();
    }
    
    public int get_size_vans() {
        return vans.size();
    }
    
    public int get_size_carros() {
        return carros.size();
    }
    
    public int get_size_motos() {
        return motos.size();
    }
    
    public void addCarreta(Carreta nova) {
       carretas.add(nova);
    }
    
    public void addVan(Van nova) {
        vans.add(nova);
    }
    
    public void addCarro(Carro novo) {
        carros.add(novo);
    }
    
    public void addMoto(Moto nova) {
        motos.add(nova);
    }

    public int getCarretasEmEntrega() {
        int cont = 0;
        for (int i = 0; i < carretas.size(); i++) {
            if (carretas.get(i).isEmEntrega() == true) {
                cont++;
            }
        }
        return cont;
    }
    
    public int getVansEmEntrega() {
        int cont = 0;
        for (int i = 0; i < vans.size(); i++) {
            if (vans.get(i).isEmEntrega() == true) {
                cont++;
            }
        }
        return cont;
    }
    
    public int getCarrosEmEntrega() {
        int cont = 0;
        for (int i = 0; i < carros.size(); i++) {
            if (carros.get(i).isEmEntrega() == true) {
                cont++;
            }
        }
        return cont;
    }
    
    public int getMotosEmEntrega() {
        int cont = 0;
        for (int i = 0; i < motos.size(); i++) {
            if (motos.get(i).isEmEntrega() == true) {
                cont++;
            }
        }
        return cont;
    }

    public void setCarretas(ArrayList<Carreta> carretas) {
        this.carretas = carretas;
    }

    public void setVans(ArrayList<Van> vans) {
        this.vans = vans;
    }

    public void setCarros(ArrayList<Carro> carros) {
        this.carros = carros;
    }

    public void setMotos(ArrayList<Moto> motos) {
        this.motos = motos;
    }
}
