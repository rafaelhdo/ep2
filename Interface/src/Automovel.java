/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rafael
 */
public class Automovel {
    private double cargaMaxima;
    private double velocidadeMedia;
    private double cargaT;
    private boolean emEntrega;
    
    public Automovel () {
        
    }

    public double getCargaMaxima() {
        return cargaMaxima;
    }

    public double getVelocidadeMedia() {
        return velocidadeMedia;
    }

    public double getCargaT() {
        return cargaT;
    }

    public boolean isEmEntrega() {
        return emEntrega;
    }
    
    protected void setCargaMaxima(double cargaMaxima) {
        this.cargaMaxima = cargaMaxima;
    }

    protected void setVelocidadeMedia(double velocidadeMedia) {
        this.velocidadeMedia = velocidadeMedia;
    }

    public void setCargaT(double cargaT) {
        this.cargaT = cargaT;
    }

    public void setEmEntrega(boolean emEntrega) {
        this.emEntrega = emEntrega;
    }
    
    
}