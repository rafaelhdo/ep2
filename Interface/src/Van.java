/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rafael
 */
public class Van extends Automovel {
    private final String combustivel;
    private final double rendimento;

    public Van() {
        combustivel = "Diesel";
        rendimento = 10;
        setCargaMaxima(3500);
        setVelocidadeMedia(80);
        setEmEntrega(false);
    }

    public double getRendimento() {
        return rendimento;
    }

    public String getCombustivel() {
        return combustivel;
    }
    
    public double calculaRendimento(int carga) {
        return getRendimento()-carga*0.001;
    }
}
