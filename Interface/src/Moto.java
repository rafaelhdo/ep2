/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rafael
 */
public class Moto extends Automovel {
    private final String combustivel;
    private final double rendimento;
    private final String combustivel2;
    private final double rendimento2;

    public Moto() {
        combustivel = "Gasolina";
        rendimento = 50;
        combustivel2 = "Álcool";
        rendimento2 = 43;
        setCargaMaxima(50);
        setVelocidadeMedia(110);
        setEmEntrega(false);
    }

    public double getRendimento() {
        return rendimento;
    }

    public String getCombustivel() {
        return combustivel;
    }
    
    public String getCombustivel2() {
        return combustivel2;
    }

    public double getRendimento2() {
        return rendimento2;
    }

    public double calculaRendimento(int carga) {
        return getRendimento()-carga*0.3;
    }
    public double calculaRendimento2(int carga) {
        return getRendimento2()-carga*0.4;
    }
}
