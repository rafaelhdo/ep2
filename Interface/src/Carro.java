/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rafael
 */
public class Carro extends Automovel {
    private final String combustivel;
    private final double rendimento;
    private final String combustivel2;
    private final double rendimento2;

    public Carro() {
        combustivel = "Gasolina";
        rendimento = 14;
        combustivel2 = "Álcool";
        rendimento2 = 12;
        setCargaMaxima(360);
        setVelocidadeMedia(100);
        setEmEntrega(false);
    }

    public double getRendimento() {
        return rendimento;
    }

    public String getCombustivel() {
        return combustivel;
    }
    
    public String getCombustivel2() {
        return combustivel2;
    }

    public double getRendimento2() {
        return rendimento2;
    }

    public double calculaRendimento(int carga) {
        return getRendimento()-carga*0.025;
    }
    public double calculaRendimento2(int carga) {
        return getRendimento2()-carga*0.0231;
    }
}
