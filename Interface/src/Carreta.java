/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rafael
 */
public class Carreta extends Automovel {
    private final String combustivel;
    private final double rendimento;

    public Carreta() {
        combustivel = "Diesel";
        rendimento = 8;
        setCargaMaxima(30000);
        setVelocidadeMedia(60);
        setEmEntrega(false);
    }

    public String getCombustivel() {
        return combustivel;
    }

    public double getRendimento() {
        return rendimento;
    }

    public double calculaRendimento(int carga) {
        return getRendimento()-carga*0.0002;
    }
    
}
